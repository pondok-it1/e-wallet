<?php

use App\Http\Controllers\ActivityController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);
Route::post('/otp/{otp:phone}/verify', [AuthController::class, 'verifyOtp']);

Route::group([
    'middleware' => ['auth']
],
    function(){
        /**
         *  ========================================================
         *  |==================== AUTHENTICATION ==================|
         *  ========================================================
         */
        Route::get('refresh-token', [AuthController::class, 'refreshToken']);
        Route::post('logout', [AuthController::class, 'logout']);

        /**
         *  ========================================================
         *  |====================== AKKTIVITAS ====================|
         *  ========================================================
         */
        Route::group(
            ['prefix' => 'activity'],
            function() {
                Route::get('/', [ActivityController::class, 'activities']);
                Route::post('/send', [ActivityController::class, 'sendToUser']);
                Route::patch('/topup', [ActivityController::class, 'topUp']);
        });
        
        /**
         *  ========================================================
         *  |========================= HOME =======================|
         *  ========================================================
         */
        Route::group([
            'prefix' => 'users'
        ], function() {
            Route::get('/', [HomeController::class, 'profile']);
            Route::post('/{phone}/search', [HomeController::class, 'search']);
        });
});