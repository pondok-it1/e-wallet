<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Activity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class ActivityController extends Controller
{
    public function activities()
    {
        $user = auth()->user();

        $activities = (Activity::orderBy('created_at', 'DESC')
                        ->with(['user:id,name,email,phone', 'toUser:id,name,email,phone'])
                        ->where('user_id', $user->id)
                        ->orWhere('to', $user->id)
                        ->get())
                        ->groupBy('date_transaction');

        if (!is_null(request('month'))) {
            $activities = (Activity::orderBy('created_at', 'DESC')
                            ->whereMonth('created_at', request('month'))
                            ->whereYear('created_at', date('Y'))
                            ->with(['user:id,name,email,phone', 'toUser:id,name,email,phone'])
                            ->where(function($query) use($user) {
                                $query->where('user_id', '=', $user->id)
                                        ->orWhere('to', '=', $user->id);
                            })
                            ->get())
                            ->groupBy('date_transaction');
        } 

        $datas = $this->allActivities($activities);

        return response()->json([
            'status' => true,
            'data' => $datas
        ]);
    }

    public function sendToUser(Request $request)
    {
        $user = auth()->user();
        $data = $request->only('to', 'nominal', 'desc', 'password');

        $validator = Validator::make($data, [
            'to'    => 'required',
            'nominal' => 'required',
            'password' => 'required',
        ], [
            'to.required'   => 'Nomor Tujuan Masih Kosong',
            'nominal.required'=> 'Nominal Belum Diisi',
            'password.required'=> 'PIN Belum Diisi'
        ]);
        
        if ($validator->fails()) {
            return response()->json([
                'status' => 'failed',
                'messages' => $validator->errors()
            ], Response::HTTP_NOT_ACCEPTABLE);
        }

        // Validasi PIN
        $credentials = [
            'email' => $user->email,
            'password' => $request->password
        ];

        if (!Auth::attempt($credentials)) {
            return response()->json([
                'status' => false,
                'message' => 'PIN Salah'
            ], Response::HTTP_NOT_ACCEPTABLE);
        }

        $toUser = User::find($data['to']);

        // Validasi user aktif
        if (!isset($toUser) || !$toUser->is_active) {
            return response()->json([
                'status' => false,
                'message' => 'Akun Yang Dituju Tidak Aktif'
             ], Response::HTTP_NOT_ACCEPTABLE);
        }

        // Validasi saldo
        if ($data['nominal'] > $user->saldo) {
            return response()->json([
                'status' => false,
                'message' => 'Saldo Anda Tidak Cukup'
            ]);
        }
        // dd($toUser->phone == $user->phone);

        if ($toUser->phone == $user->phone) return response()->json([
            'data' => [
                'status' => false,
                'message' => 'Maaf, Tidak Bisa Mengirim Ke Diri Sendiri'
            ]
        ], Response::HTTP_NOT_ACCEPTABLE);

        $saldoMe = $user->saldo - $data['nominal'];
        $saldoToUser = $toUser->saldo + $data['nominal'];
        $data['date_transaction'] = now();

        // Proses Transaction dan Update Saldo
        $transaction = $user->activities()->create($data);
        $transaction['to_user'] = collect($toUser)->only('id', 'name', 'email', 'phone');
        
        $transaction->user()->update(['saldo' => $saldoMe]);
        $transaction->toUser()->update(['saldo' => $saldoToUser]);

        return response()->json([
            'status' => true,
            'message' => 'Transaksi Berhasil',
            'data' => $transaction
        ]);
    }

    public function topUp(Request $request)
    {
        $user = User::find(Auth::id());
        $data = $request->only('nominal', 'password');

        $validator = Validator::make($data, [
            'nominal' => 'required',
            'password' => 'required'
        ], [
            'nominal.required' => 'Nominal Harus Diisi',
            'password.required' => 'PIN Belum Diisi'
        ]);

        if ($validator->fails()) return response()->json([
            'status' => false,
            'message' => $validator->errors()
        ], Response::HTTP_NOT_ACCEPTABLE);

        // Validasi PIN
        $credentials = [
            'email' => $user->email,
            'password' => $request->password
        ];

        if (!Auth::attempt($credentials)) {
            return response()->json([
                'status' => false,
                'message' => 'PIN Salah'
            ], Response::HTTP_NOT_ACCEPTABLE);
        } // End validasi PIN

        $currentSaldo = is_null($user->saldo) ? 0 : $user->saldo;
        $updateSaldo = $currentSaldo + $data['nominal'];

        $user->update(['saldo' => $updateSaldo]);

        $dataAct = $data;
        $dataAct['to'] = $user->id;
        $dataAct['desc'] = 'Top Up';
        $dataAct['date_transaction'] = now();
        $transaction = $user->activities()->create($dataAct);

        return response()->json([
            'status' => true,
            'message' => 'Top Up Berhasil',
            'data' => $transaction
        ]);
    }

    private function allActivities($datas)
    {
        $newData = [];
        $userId = auth()->id();
        
        foreach ($datas as $month => $activity) {
            // $data = $data[$month];
            $transactions = [];
            foreach ($activity as $data) {
                $isTopUp = $userId == $data->to ? true : false;
    
                $date = date('d, F Y', strtotime($month));
                $transaction = [
                    'id' => $data->id,
                    'label_name' => $isTopUp ? 'Isi Saldo' : 'Saldo Keluar',
                    'desc' => $data->desc,
                    'status' => true,
                    'amount_label' => 'Rp '. number_format($data->nominal,0,'.','.'),
                    'amount' => $data->nominal
                ];

                array_push($transactions, [
                    // 'id' => $data->id,
                    // 'date' => $date,
                    'transaction' => $transaction
                ]);
            }

            array_push($newData, [
                'id' => $data->id,
                'date' => $date,
                'transaction' => $transactions
            ]);

        }

        return $newData;
    }
}
