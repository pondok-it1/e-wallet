<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function profile()
    {
        return response()->json([
            'status' => true,
            'data' => auth()->user()
        ]);
    }

    public function search($phone)
    {
        $users = User::where('phone', $phone)->get();

        return response()->json([
            'status' => true,
            'data' => $users
        ]);
    }
}
