<?php

namespace App\Http\Controllers;

use App\Mail\EwalletMail;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login','register','verifyOtp']]);
    }

    public  function register(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'name'  => 'required',
            'email' => 'required|email|unique:users',
            'phone' => 'required|unique:users,phone',
            'password' => 'required|confirmed'
        ], [
            'name.required'     => 'Nama Harus Diisi',
            'email.required'    => 'Email Masih Kosong',
            'email.email'       => 'Format Email Tidak Sesuai',
            'email.unique'      => 'Email Sudah Ada',
            'phone.required'    => 'Nomor Wa Harus Diisi',
            'phone.unique'      => 'Nomor Wa Sudah Ada',
            'password.required' => 'Password Harus Diisi',
            'password.confirmed' => 'Konfirmasi Password Tidak Sesuai' 
        ]);
        
        // Validasi data yg dikirim
        if ($validator->fails()) {
            return response()->json([
                'status' => 'failed',
                'messages' => $validator->errors()
            ], Response::HTTP_NOT_ACCEPTABLE);
        }
        
        $data = $request->only('name', 'email', 'phone', 'password');
        $data['password'] = Hash::make($request->password);
        $user = User::create($data);

        $otp = $user->otp()->create(['code' => random_int(100000, 999999)]);
        $data['otp'] = $otp;

        $phone = $user->phone;
        $message = "Hallo $user->name \n\nIni Adalah Kode OTP Anda \n$otp->code";
        $this->waOtp($phone, $message);
        // Mail::to($request->email)->send(new EwalletMail($data));

        return response()->json([
            'status' => true,
            'message' => 'Data Anda Sudah Tersimpan',
            'data' => $user
        ], Response::HTTP_OK);
    }

    public function login(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required'
        ], [
            'email.required'    => 'Email Kosong',
            'password.required'    => 'Password Kosong',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'failed',
                'messages' => $validator->errors()
            ], Response::HTTP_NOT_ACCEPTABLE);
        }

        $credentials = $request->only('email', 'password');

        if (!$token = auth()->attempt($credentials)) {
            return response()->json([
                'status' => false,
                'message' => 'Email atau Password Tidak Sesuai'
            ], Response::HTTP_NOT_ACCEPTABLE);
        }

        $user = User::where('email', $request->email)->first();

        if (!$user->is_active) {
            return response()->json([
                'status' => false,
                'message' => 'Akun Anda Belum Aktif'
            ], Response::HTTP_NOT_ACCEPTABLE);
        }

        return response()->json([
            'status' => true,
            'message' => 'Login Berhasil',
            'data' => $user,
            'token' => $token
        ]);
    }

    public function logout()
    {
        auth()->logout();

        return response()->json([
            'status' => true,
            'message' => 'Logout Berhasil'
        ]);
    }

    public function refreshToken()
    {
        $newToken = auth()->refresh();

        return response()->json([
            'status' => true,
            'message' => 'Refresh Token Berhasil',
            'token' => $newToken
        ]);
    }

    public function verifyOtp(Request $request,$phone)
    {
        $data = $request->only('otp');
        $user = User::with('otp')->where('phone', $phone)->first();
        $validator = Validator::make($data, ['otp' => 'required',], ['otp.required' => 'OTP Belum Diisi']);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'failed',
                'messages' => $validator->errors()
            ], Response::HTTP_NOT_ACCEPTABLE);
        }

        if (!isset($user)) {
            return  response()->json([
                'data' => [
                    'status' => false,
                    'message' => 'Nomor Telepon Tidak Sesuai'
                ]
            ], Response::HTTP_NOT_ACCEPTABLE);
        }

        $otp = $user->otp->where('code', $request->otp)->first();

        if (!isset($otp)) return response()->json([
            'data' => [
                'status' => false,
                'message' => 'Kode OTP Tidak Sesuai'
            ]
        ], Response::HTTP_NOT_ACCEPTABLE);
        
        if ($user->is_active || $otp->is_verified) {
            return  response()->json([
                'data' => [
                    'status' => false,
                    'message' => 'Kode OTP Telah Expire. Gunakan Kode Yang Lain'
                ]
            ], Response::HTTP_NOT_ACCEPTABLE);
        }

        $otp->update(['is_verified' => true]);
        $otp->user()->update(['is_active' => true]);

        return response()->json([
            'data'  => [
                'status' => true,
                'message' => 'Verifikasi Berhasil. Akun Anda Telah Diaktifkan'
            ]
        ], Response::HTTP_NOT_ACCEPTABLE);
    }
}
