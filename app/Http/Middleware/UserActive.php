<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class UserActive
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $user = auth()->user();
        dd('oke');

        if (!$user->is_active) {
            return response()->json([
                'status' => false,
                'message' => 'Akun Anda Belum Aktif'
            ]);
        }

        return $next($request);
    }
}
